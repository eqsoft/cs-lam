#!/usr/bin/env bash
. .env

docker build . -t registry.gitlab.com/eqsoft/cs-lam:${STACK_ID}
