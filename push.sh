#!/usr/bin/env bash
. .env
docker login registry.gitlab.com
docker push registry.gitlab.com/eqsoft/cs-lam:${STACK_ID}
